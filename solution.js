
/*
pseudo code
1. break case (because recursion)
2. pick a pivot (end)
3. find smaller and larger numbers
4. recurse smaller, larger
*/

// "spread operator"
// const arr0 = [324, 4567, 9]
// const arr1 = [324, 4567, 9]
// const arr2 = [...arr1, ...arr0]; // [324, 4567, 9, 324, 4567, 9]


function quicksort(arr) {
    if (arr.length <= 1) { return arr; }

    const pivot = arr[arr.length - 1];
    const left = [];
    const right = [];

    // for i in range(len(arr)):
    for (let i = 0; i < arr.length - 1; i += 1) {
        if (arr[i] > pivot) {
            right.push(arr[i]);
        } else {//} if (arr[i] < pivot) {
            left.push(arr[i]);
        }
    }

    const sortedLeft = quicksort(left);
    const sortedRight = quicksort(right);

    const res = [...sortedLeft, pivot, ...sortedRight]
    return res;

    // above similar to merge sort
    // result = []
    // while length(left) > 0 or length(right) > 0:
    //     if left[0] > right[0]
    //         result.append(right[0])
}

module.exports = {
    quicksort
}
